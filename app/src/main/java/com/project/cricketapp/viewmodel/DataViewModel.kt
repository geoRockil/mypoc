package com.project.cricketapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.project.cricketapp.network.model.ResMatchData
import com.project.cricketapp.network.repo.DataRepository


/**
 * Created by George on 02/03/24.
 */
class DataViewModel :ViewModel() {
    private var dataRepository: DataRepository = DataRepository()
    private var getMatchDataCallbackLiveData: LiveData<ResMatchData>

    init {
        getMatchDataCallbackLiveData = MutableLiveData()
    }

    fun getMatchData(): LiveData<ResMatchData> {
        getMatchDataCallbackLiveData = dataRepository.getMatchData()
        return getMatchDataCallbackLiveData
    }
}

class DataViewModelFactory :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DataViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DataViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class !!!!  Expected ${DataViewModel::class.java.name}")
    }
}