package com.project.cricketapp.network.model.base

import com.project.cricketapp.shelf.Error


/**
 * Created by George on 03/03/24.
 */
abstract class BaseApiResponse {
    var statusCode: Int = 400
    var success: Boolean = false
    var message: String = Error.UNEXPECTED
}