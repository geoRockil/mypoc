package com.project.cricketapp.network.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.project.cricketapp.network.DataModule
import com.project.cricketapp.network.model.ResMatchData
import com.project.cricketapp.shelf.base.BaseRepository
import com.project.cricketapp.shelf.Error
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by George on 03/03/24.
 */
class DataRepository: BaseRepository() {

    fun getMatchData(): LiveData<ResMatchData> {

        val data = MutableLiveData<ResMatchData>()

        DataModule.Network.matchData().getMatchData().enqueue(object : Callback<ResMatchData> {

                var responseStbStatus = ResMatchData()

                override fun onFailure(call: Call<ResMatchData>, t: Throwable) {
                    Log.d("Api====>","failure onFailure")
                    responseStbStatus.statusCode = 500
                    responseStbStatus.message = Error.FAILED_REQUEST
                    data.postValue(responseStbStatus)
                }

                override fun onResponse(
                    call: Call<ResMatchData>,
                    response: Response<ResMatchData>
                ) {
                    if (response.isSuccessful) {
                        Log.d("Api====>","success")
                        if(response.body()!=null){
                            responseStbStatus = response.body()!!
                            responseStbStatus.statusCode = 200
                            data.postValue(responseStbStatus)
                        }else{
                            responseStbStatus.statusCode = 500
                            responseStbStatus.message = Error.UNEXPECTED
                            data.postValue(responseStbStatus)
                            Log.d("Api====>","error")
                        }

                    } else {
                        responseStbStatus.statusCode = 500
                        responseStbStatus.message = Error.FAILED_REQUEST
                        data.postValue(responseStbStatus)
                        Log.d("Api====>","error")

                    }

                }
            })
        return data
    }

}