package com.project.cricketapp.network

import com.project.cricketapp.network.model.ResMatchData
import retrofit2.http.GET
import retrofit2.Call


/**
 * Created by George on 02/03/24.
 */
internal interface Api {

    @GET("/bins?id=mlY28vnsf")
    fun getMatchData(
    ): Call<ResMatchData>
}