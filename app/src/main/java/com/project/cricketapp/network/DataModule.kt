package com.project.cricketapp.network

import com.project.cricketapp.network.AppConfig.MOCK_API
import com.project.cricketapp.viewmodel.DataViewModel
import com.project.cricketapp.viewmodel.DataViewModelFactory


/**
 * Created by George on 02/03/24.
 */
class DataModule {

    /**
     * All network API consumptions begins here
     * */
    object Network {

        /**This service will use dynamic base url from appConfig*/
        private fun <S> setServices(serviceClass: Class<S>): S {
            return RetrofitFactory.retrofit(MOCK_API).create(serviceClass)
//            return RetrofitFactory.retrofit(BASE_API).create(serviceClass)
        }

        /** Get access to all user and user account related API*/
        internal fun matchData(): Api {
            return setServices(Api::class.java)
        }

        val dataViewModel: DataViewModel =
            DataViewModelFactory().create(
                DataViewModel::class.java
            )
    }

}