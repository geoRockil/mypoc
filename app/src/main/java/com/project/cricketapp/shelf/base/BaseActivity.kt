package com.project.cricketapp.shelf.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import com.project.cricketapp.network.DataModule


/**
 * Created by George on 03/03/24.
 */
open class BaseActivity : AppCompatActivity() {

    protected var dataViewModel = DataModule.Network.dataViewModel

    fun addAnimation(): Bundle {
        return ActivityOptionsCompat.makeCustomAnimation(
            this,
            android.R.anim.fade_in, android.R.anim.fade_out
        ).toBundle()!!
    }

}