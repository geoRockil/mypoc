package com.project.cricketapp.shelf


/**
 * Created by George on 03/03/24.
 */
object Error {
    const val UNEXPECTED =
        "We're very sorry, but something unexpected has happened. \n The details of this problem have been logged !!"

    const val FAILED_REQUEST = "That was something unexpected !"
}