package com.project.cricketapp.shelf

import com.project.cricketapp.network.model.MatchDetails
import com.project.cricketapp.network.model.Team


/**
 * Created by George on 03/03/24.
 */
object Utils {
    fun returnTotalRun(team: Team): Int {
        return  team.players.sumBy { it.runs }
    }

    fun returnTotalOvers(team: Team): Double {
        return team.bowlers.sumOf { it.overs.toDouble()  }
    }

    fun calculateEconomy(runs: Float, over: Float): Float {
        return runs / over
    }

    fun calculateStrikeRate(runs: Float, balls: Float): Float {
        return (runs / balls)*100
    }

    fun returnWickets(matchDetails: MatchDetails, team: String): Int {
        return matchDetails.fallOfWickets
            .firstOrNull { it.team == team }?.wickets?.size ?: 0
    }

    fun calculateWinner(totalRunA: Int, totalRunB: Int, teams: List<Team>): String {
        return if (totalRunA > totalRunB) {
            "${teams[0].name} won by ${totalRunA - totalRunB} runs"
        } else {
            "${teams[1].name} won by ${totalRunB - totalRunA} runs"
        }
    }

}