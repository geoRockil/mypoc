package com.project.cricketapp.shelf.base

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


/**
 * Created by George on 03/03/24.
 */
open class BaseRepository {

    fun <T> toModelClass(string: String, classOfT: Class<T>): T {
        return Gson().fromJson(string, classOfT)
    }

    inline fun <reified T> fromJson(json: String): T? {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }

    fun <T> mapToObject(map: Map<String, Any?>?, type: Class<T>): T? {
        if (map == null) return null

        val json = Gson().toJson(map)
        return Gson().fromJson(json, type)
    }
}