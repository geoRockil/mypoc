package com.project.cricketapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.project.cricketapp.R
import com.project.cricketapp.databinding.ItemBowlersBinding
import com.project.cricketapp.network.model.Bowler
import com.project.cricketapp.shelf.Utils


/**
 * Created by George on 03/03/24.
 */
class BowlerAdapter(var mList: List<Bowler>): RecyclerView.Adapter<BowlerAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemBowlersBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBowlersBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            with(mList[position]){
                binding.tvBowlName.text = name
                binding.tvBowlOver.text = "$overs"
                binding.tvBowlMaiden.text = "0"
                binding.tvBowlRun.text = "$runsConceded"
                binding.tvBowlWicket.text = "$wickets"
                binding.tvBowlEco.text = Utils.calculateEconomy(runsConceded.toFloat(),overs.toFloat()).toString()
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }
}