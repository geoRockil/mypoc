package com.project.cricketapp.ui.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.project.cricketapp.databinding.ActivityTeamDetailBinding
import com.project.cricketapp.network.AppConfig.JSON_DATA
import com.project.cricketapp.network.model.MatchDetails
import com.project.cricketapp.shelf.Utils
import com.project.cricketapp.shelf.Utils.returnTotalOvers
import com.project.cricketapp.shelf.Utils.returnTotalRun
import com.project.cricketapp.shelf.Utils.returnWickets
import com.project.cricketapp.shelf.base.BaseActivity
import com.project.cricketapp.ui.adapter.BowlerAdapter
import com.project.cricketapp.ui.adapter.BatterAdapter


/**
 * Created by George on 03/03/24.
 */
class DetailActivity:BaseActivity() {
    private lateinit var binding: ActivityTeamDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTeamDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        intent.getStringExtra(JSON_DATA).let {
            val matchDetails = Gson().fromJson(it, MatchDetails::class.java)
            setUpUI(matchDetails)
        }
    }

    private fun setUpUI(matchDetails: MatchDetails){
        binding.tvMatchType.text = "${matchDetails.format} Premier League"
        val totalRunsA = returnTotalRun(matchDetails.teams[0])
        val totalRunsB = returnTotalRun(matchDetails.teams[1])
        binding.tvWinBy.text = Utils.calculateWinner(totalRunsA, totalRunsB, matchDetails.teams)

        matchDetails.teams[0].let {
            binding.tvTeamA.text = it.name
            binding.tvTeamNameA.text = it.name
            val totalWickets = returnWickets(matchDetails,it.name)
            binding.tvRunA.text = "$totalRunsA/$totalWickets"
            binding.tvOverA.text = "(${returnTotalOvers(it)})"
            if (it.name == matchDetails.toss) {
                binding.tvTossA.visibility = View.VISIBLE
                binding.tvTossA.text = "${it.name} win toss elect ${matchDetails.toss_decision}"
            }

            binding.rvBattersA.layoutManager = LinearLayoutManager(this)
            val batterAdapterA = BatterAdapter(it.players)
            binding.rvBattersA.adapter = batterAdapterA

            binding.rvBowlA.layoutManager = LinearLayoutManager(this)
            val bowlerAdapterA = BowlerAdapter(it.bowlers)
            binding.rvBowlA.adapter = bowlerAdapterA
        }

        matchDetails.teams[1].let {
            binding.tvTeamB.text = it.name
            binding.tvTeamNameB.text = it.name
            val totalWickets = returnWickets(matchDetails,it.name)
            binding.tvRunB.text = "$totalRunsB/$totalWickets"
            binding.tvOverB.text = "(${returnTotalOvers(it)})"
            if (it.name == matchDetails.toss) {
                binding.tvTossB.visibility = View.VISIBLE
                binding.tvTossB.text = "${it.name} win toss elect ${matchDetails.toss_decision}"
            }

            binding.rvBattersB.layoutManager = LinearLayoutManager(this)
            val batterAdapterB = BatterAdapter(it.players)
            binding.rvBattersB.adapter = batterAdapterB

            binding.rvBowlB.layoutManager = LinearLayoutManager(this)
            val bowlerAdapterB = BowlerAdapter(it.bowlers)
            binding.rvBowlB.adapter = bowlerAdapterB
        }
    }

}