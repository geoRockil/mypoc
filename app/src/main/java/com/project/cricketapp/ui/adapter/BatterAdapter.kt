package com.project.cricketapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.project.cricketapp.databinding.ItemBatterBinding
import com.project.cricketapp.network.model.Player
import com.project.cricketapp.shelf.Utils


/**
 * Created by George on 03/03/24.
 */
class BatterAdapter(var mList: List<Player>) : RecyclerView.Adapter<BatterAdapter.ViewHolder>(){
    inner class ViewHolder(val binding: ItemBatterBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemBatterBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(mList[position]) {
                binding.tvBatName.text = name
                binding.tvBatRun.text = "$runs"
                binding.tvBatBall.text = "$balls"
                binding.tvBat4.text = "$fours"
                binding.tvBat6.text = "$sixes"
                binding.tvStrikeRate.text = Utils.calculateStrikeRate(runs.toFloat(),balls.toFloat()).toString()

                if (dismissal.fielder == null){
                    binding.tvBatDiss.text = "${dismissal.type} ${dismissal.bowler}"

                }else {
                    binding.tvBatDiss.text = "${dismissal.type} ${ dismissal.fielder}      b ${dismissal.bowler}"
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }


}