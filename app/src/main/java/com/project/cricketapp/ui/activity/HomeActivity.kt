package com.project.cricketapp.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.project.cricketapp.databinding.ActivityHomeBinding
import com.project.cricketapp.network.AppConfig.JSON_DATA
import com.project.cricketapp.network.model.MatchDetails
import com.project.cricketapp.shelf.Utils.calculateWinner
import com.project.cricketapp.shelf.Utils.returnTotalOvers
import com.project.cricketapp.shelf.Utils.returnTotalRun
import com.project.cricketapp.shelf.Utils.returnWickets
import com.project.cricketapp.shelf.base.BaseActivity


/**
 * Created by George on 02/03/24.
 */
class HomeActivity : BaseActivity() {
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getMatchData()
    }

    private fun startDetailActivity(matchDetails: MatchDetails){
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(JSON_DATA, Gson().toJson(matchDetails))
        startActivity(intent,addAnimation())
    }

    private fun getMatchData() {
        dataViewModel.getMatchData()
            .observe(this){
                binding.pbHome.visibility = View.GONE
                if (it.statusCode == 200){
                    setData(it.matchDetails)
                }else{
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun setData(data: MatchDetails) {
        if (data.teams.size>=2){
            binding.tvMatchType.text = "${data.format} Premier League"
            binding.tvTeamA.text =  "${data.teams[0].name}"
            binding.tvTeamB.text =  "${data.teams[1].name}"
            val totalRunA = returnTotalRun(data.teams[0])
            val totalRunB = returnTotalRun(data.teams[1])
            binding.tvRunA.text = "$totalRunA/${returnWickets(data,data.teams[0].name)}"
            binding.tvRunB.text = "$totalRunB/${returnWickets(data,data.teams[1].name)}"
            binding.tvOverA.text = "(${returnTotalOvers(data.teams[0])})"
            binding.tvOverB.text = "(${returnTotalOvers(data.teams[1])})"
            binding.tvWin.text = calculateWinner(totalRunA,totalRunB,data.teams)
        }

        binding.cvMatch.setOnClickListener {
            startDetailActivity(data)
        }
    }

}
